<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <title>Login</title>
</head>
<body>

<div class="container" style="margin-top: 10%">

<form method="POST" action="/login" class="mt-10" style="width: 40%; margin: 0 auto">
    @csrf
    <div class="form-outline mb-4">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" name="email" id="email">

        @error('email')
            <p>{{$message}}</p>
        @enderror
    </div>


    <div class="form-outline mb-4">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" class="form-control" id="password">

        @error('password')
            <p>{{$message}}</p>
        @enderror
    </div>

    <div>
        <button type="submit" class="btn btn-primary btn-block mb-4">Submit</button>
    </div>
</form>

</div>
</body>
</html>
